package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        try {
            inputNumbers.sort(Integer::compareTo);
        } catch (Throwable th) {
            throw new CannotBuildPyramidException();
        }
        int rowNumber = 0;
        int listSize = inputNumbers.size();
        while (listSize != 0){
            listSize -= ++rowNumber;
            if (listSize < 0) {
                throw new CannotBuildPyramidException();
            }
        }
        int columnNumber = rowNumber * 2 - 1;
        int[][] pyramid = new int[rowNumber][columnNumber];
        int middle = (columnNumber) / 2;
        int currentIndex = 0;
        for (int i = 0; i < rowNumber; i++) {
            for (int j = 0; j <= i; j++) {
                pyramid[i][middle - i + j * 2] = inputNumbers.get(currentIndex);
                currentIndex++;
            }
        }
        return pyramid;
    }
}
