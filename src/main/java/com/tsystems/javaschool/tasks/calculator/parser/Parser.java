package com.tsystems.javaschool.tasks.calculator.parser;

import com.tsystems.javaschool.tasks.calculator.operation.*;
import com.tsystems.javaschool.tasks.calculator.operation.Number;

import java.text.ParseException;

public class Parser {

    public Evaluable parse(String expression) throws ParseException {
        String expressionWithoutSpaces = expression.replaceAll(" +", "");
        return parse(expressionWithoutSpaces, 0);
    }

    private Evaluable parse(String expression, int offset) throws ParseException {
        if (expression.equals("")) {
            throw new ParseException("Empty part of expression", offset);
        }
        int lastIndex = expression.length() - 1;
        if (expression.indexOf(")") == lastIndex && findOpeningBracketIndex(expression, lastIndex) == 0) {
            return parse(expression.substring(1, lastIndex), offset + 1);
        }
        int openingBracketIndex = expression.length();
        int closingBracketIndex;
        int plusIndex;
        int minusIndex;
        int divisionIndex = -1;
        int multiplicationIndex = -1;
        do {
            int rightBorderOfSearch = openingBracketIndex - 1; // inclusive
            int leftBorderOfSearch = 0; // exclusive
            closingBracketIndex = expression.lastIndexOf(')', openingBracketIndex - 1);
            if (closingBracketIndex > 0) {
                openingBracketIndex = findOpeningBracketIndex(expression, closingBracketIndex);
                if (openingBracketIndex < 0) {
                    throw new ParseException("Extra closing bracket", offset + closingBracketIndex);
                }
                leftBorderOfSearch = closingBracketIndex;
            }
            plusIndex = lastIndexInOfFromTo(expression, '+', leftBorderOfSearch, rightBorderOfSearch);
            minusIndex = lastIndexInOfFromTo(expression, '-', leftBorderOfSearch, rightBorderOfSearch);
            if (plusIndex > 0 || minusIndex > 0) {
                break;
            }
            if (divisionIndex < 0) {
                divisionIndex = lastIndexInOfFromTo(expression, '/', leftBorderOfSearch, rightBorderOfSearch);
            }
            if (multiplicationIndex < 0) {
                multiplicationIndex = lastIndexInOfFromTo(expression, '*', leftBorderOfSearch, rightBorderOfSearch);
            }
        } while (closingBracketIndex > 0);
        if (plusIndex > 0 && plusIndex > minusIndex) {
            return new Plus(
                    parse(expression.substring(0, plusIndex), 0),
                    parse(expression.substring(plusIndex + 1), plusIndex + 1)
            );
        } else if (minusIndex > 0) {
            return new Minus(
                    parse(expression.substring(0, minusIndex), 0),
                    parse(expression.substring(minusIndex + 1), minusIndex + 1)
            );
        } else if (multiplicationIndex > 0 && multiplicationIndex > divisionIndex) {
            return new Multiplication(
                    parse(expression.substring(0, multiplicationIndex), 0),
                    parse(expression.substring(multiplicationIndex + 1), multiplicationIndex + 1)
            );
        } else if (divisionIndex > 0) {
            return new Division(
                    parse(expression.substring(0, divisionIndex), 0),
                    parse(expression.substring(divisionIndex + 1), divisionIndex + 1)
            );
        } else {
            try {
                return new Number(Double.parseDouble(expression));
            } catch (NumberFormatException ex) {
                throw new ParseException("Failed to parse value", offset);
            }
        }
    }

    private int findOpeningBracketIndex(String expression, int closingBracketIndex) {
        int depth = 0;
        for (int i = closingBracketIndex - 1; i >= 0; i--) {
            if (expression.charAt(i) == '(') {
                if (depth == 0) {
                    return i;
                }
                depth--;
            } else if (expression.charAt(i) == ')') {
                depth++;
            }
        }
        return -1;
    }

    private int lastIndexInOfFromTo(String expression, int ch, int from, int to) {
        int index = expression.lastIndexOf(ch, to);
        if (index <= from) {
            return -1;
        }
        return index;
    }
}