package com.tsystems.javaschool.tasks.calculator.operation;

public class Plus extends AbstractBinaryOperator {

    public Plus(Evaluable left, Evaluable right) {
        super(left, right);
    }

    @Override
    public double evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
