package com.tsystems.javaschool.tasks.calculator.operation;

public abstract class AbstractBinaryOperator implements Evaluable {

    protected final Evaluable left;
    protected final Evaluable right;

    AbstractBinaryOperator(Evaluable left, Evaluable right) {
        this.left = left;
        this.right = right;
    }
}
