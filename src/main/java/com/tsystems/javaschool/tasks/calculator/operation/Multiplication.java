package com.tsystems.javaschool.tasks.calculator.operation;

public class Multiplication extends AbstractBinaryOperator {

    public Multiplication(Evaluable left, Evaluable right) {
        super(left, right);
    }

    @Override
    public double evaluate() {
        return left.evaluate() * right.evaluate();
    }
}
