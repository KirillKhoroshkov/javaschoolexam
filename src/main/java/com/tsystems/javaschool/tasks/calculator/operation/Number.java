package com.tsystems.javaschool.tasks.calculator.operation;

public class Number implements Evaluable {

    private final double value;

    public Number(double value) {
        this.value = value;
    }

    @Override
    public double evaluate() {
        return value;
    }
}
