package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.operation.Evaluable;
import com.tsystems.javaschool.tasks.calculator.parser.Parser;

import java.text.DecimalFormat;
import java.text.ParseException;

public class Calculator {

    private final Parser parser = new Parser();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        try {
            Evaluable evaluable = parser.parse(statement);
            double result = evaluable.evaluate();
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) {
                return null;
            }
            return new DecimalFormat("#.####").format(result);
        } catch (ParseException ex) {
            return null;
        }
    }
}
