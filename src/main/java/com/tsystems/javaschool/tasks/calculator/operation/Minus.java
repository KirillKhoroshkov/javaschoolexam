package com.tsystems.javaschool.tasks.calculator.operation;

public class Minus extends AbstractBinaryOperator {

    public Minus(Evaluable left, Evaluable right) {
        super(left, right);
    }

    @Override
    public double evaluate() {
        return left.evaluate() - right.evaluate();
    }
}
