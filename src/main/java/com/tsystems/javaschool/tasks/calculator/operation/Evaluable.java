package com.tsystems.javaschool.tasks.calculator.operation;

public interface Evaluable {

    double evaluate();
}
