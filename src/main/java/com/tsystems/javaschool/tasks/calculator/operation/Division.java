package com.tsystems.javaschool.tasks.calculator.operation;

public class Division extends AbstractBinaryOperator {

    public Division(Evaluable left, Evaluable right) {
        super(left, right);
    }

    @Override
    public double evaluate() {
        return left.evaluate() / right.evaluate();
    }
}
