package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) {
            return true;
        }
        int xIndex = 0;
        int xSize = x.size();
        for (Object o : y) {
            if (x.get(xIndex).equals(o)) {
                xIndex++;
            }
            if (xIndex >= xSize) {
                return true;
            }
        }
        return false;
    }
}
